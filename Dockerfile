FROM node:12.16.1-alpine

RUN apk update
RUN apk add wget libstdc++ openblas-dev python3 python3-dev py-pip openssl-dev libffi-dev build-base cmake zlib libjpeg py3-pillow py3-numpy
RUN rm -rf /var/cache/apk/*

### DLIB ###
WORKDIR /

RUN pip install --upgrade pip
RUN pip3 install --upgrade pip

RUN cd ~ && \
  wget http://dlib.net/files/dlib-19.17.tar.bz2 && \
  tar -xjf dlib-19.17.tar.bz2 && \
  cd dlib-19.17 && \
  python3 setup.py install --set USE_SSE4_INSTRUCTIONS=true

RUN rm -rf ~/dlib-19.17 dlib-19.17.tar.bz2

ADD ./requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

### GOOGLE SDK ###
# When need google SKD, uncomment this
# RUN wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz --no-check-certificate \
#   && tar zxvf google-cloud-sdk.tar.gz \
#   && rm google-cloud-sdk.tar.gz \
#   && ./google-cloud-sdk/install.sh --usage-reporting=true --path-update=true
# ENV PATH /google-cloud-sdk/bin:/google-cloud-sdk/platform/google_appengine/:$PATH
# ENV APPENGINE /google-cloud-sdk/platform/google_appengine/
# ENV GOPATH /code/golibs
# RUN yes | gcloud components update

### REMOVE UNUSED PACKAGES ###
RUN apk del wget openblas-dev python3-dev openssl-dev libffi-dev build-base cmake
RUN rm -rf /var/cache/apk/*
RUN rm -rf /tmp/*
# RUN mkdir -p /code/golibs

# Setup locale. This prevents Python 3 IO encoding issues.
ENV LANG C.UTF-8
# Make stdout/stderr unbuffered. This prevents delay between output and cloud
# logging collection.
ENV PYTHONUNBUFFERED 1


### APP ###
# Setup the app working directory
RUN ln -s /home/vmagent/app /app
WORKDIR /app

# Port 8080 is the port used by Google App Engine for serving HTTP traffic.
EXPOSE 8080
ENV PORT 8080